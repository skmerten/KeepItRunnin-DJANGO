from django.shortcuts import render
from django.http import HttpRequest
from django.http import Http404
from django.template import RequestContext
from datetime import datetime, timedelta
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from vehicles.models import Vehicle
from parts.models import Part, Part_History
from maintenance.models import Maintenance, Maintenance_Record
from maintenance.forms import NewMaintenance, NewMaintenanceHistory, ChooseMaintenance
from app.forms import NewVehicle, NewPart, PartHistory, ChoosePart ,NewUserForm, BootstrapAuthenticationForm

# Needs Updates
@login_required(login_url='/login')
def partHome(request):
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/partsHome.html',
        {
            'title':'Parts',
            'year':datetime.now().year,
            'parts': Part.objects.filter(maintenance__vehicle__user = request.user),
            'partOptions' : True
        }
    )


# Needs Updates
@login_required(login_url='/login')
def viewPart(request):
    part = Part.objects.all()
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/viewPart.html',
        {
            'title':'View All Parts',
            'year':datetime.now().year,
            'parts': part,
            'partOptions' : True
        }
    )


# Needs Updates
@login_required(login_url='/login')
def addPart(request):
    if request.method == 'POST':
        form = NewPart(request.user, request.POST)
        if form.is_valid():
            id = request.POST['id']
            if id:
                record = Part.objects.get(pk = id)
                record.maintenance = Maintenance.objects.get(id = request.POST['maintenance'])
                record.part_name = request.POST['part_name']
                record.part_description = request.POST['part_description']
                record.quantity = request.POST['quantity']
                record.comments = request.POST['comments']
                record.save()
            else:
                form.save()
                
            # render Home Page
#            assert isinstance(request, HttpRequest)
#            return render(
#                request,
#                'app/partsHome.html',
#                {
#                    'title':'Parts',
#                    'year':datetime.now().year,
#                    'parts': Part.objects.filter(maintenance__vehicle__user = request.user),
#                    'partOptions' : True
#                }
#            )
    part = Part.objects.all()
    assert isinstance(request, HttpRequest)
    return render( 
        request,
        'app/viewPart.html',
        {
            'title':'View All Parts',
            'year':datetime.now().year,
            'parts': part,
            'partOptions' : True
        }
    )

# Needs Updates
@login_required(login_url='/login')
def viewPartHist(request):
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/viewMaintHist.html',
        {
            'title':'Part Purchase History''',
            'year':datetime.now().year,
            'maintenanceHistory': Maintenance_Record.objects.all(),
            'partOptions' : True
        }
    )

# Needs Updates
@login_required(login_url='/login')
def logPart(request):
    if request.method == 'POST':
        form = PartHistory(request.user, request.POST)
        if form.is_valid():
            form.save()
            part = Part.objects.get(id = request.POST['part'])
            part.status = True
            part.save()
                      
            part_hist = Part_History.objects.filter(part__maintenance__vehicle__user = request.user)
            assert isinstance(request, HttpRequest)
            return render(
                request,
                'app/viewPartHist.html',
                {
                    'title':'Part Purchase History',
                    'year':datetime.now().year,
                    'part':part_hist,
                    'partOptions' : True
                }
            )

    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/logPart.html',
        {
            'title':'Purchased Part',
            'year':datetime.now().year,
            'form': PartHistory(user = request.user),
            'partOptions' : True
        }
    )

# Needs Updates
@login_required(login_url='/login')
def choosePart(request):
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/choosePart.html',
        {
            'title':'Select Part',
            'year':datetime.now().year,
            'part': ChoosePart(user = request.user),
            'partOptions' : True
        }
    )

# Needs Updates
@login_required(login_url='/login')
def editPart(request):
    pk = request.POST['part']
    parts = Part.objects.get(id = pk)
    form = NewPart(user = request.user, initial={
            'id': pk,
            'maintenance': parts.maintenance,
            'part_name': parts.part_name,
            'part_description': parts.part_description, 
            'quantity': parts.quantity,
            'comments':parts.comments,
            'status':parts.status
        })

    
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/addPart.html',
        {
            'title':'Edit Part',
            'year':datetime.now().year,
            'newPart': form,
            'partOptions' : True
        }
    )

@login_required(login_url='/login')
def viewPartHist(request):
    part_hist = Part_History.objects.filter(part__maintenance__vehicle__user = request.user)
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/viewPartHist.html',
        {
            'title':'Part Purchase History',
            'year':datetime.now().year,
            'part':part_hist,
            'partOptions' : True
        }
    )